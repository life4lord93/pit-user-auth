package com.pit.auth.repository;

import com.pit.auth.account.model.Account;
import com.pit.auth.account.repository.AccountRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class AccountRepositoryTest {
    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void getUserListTest(){
        Account account = accountRepository.findByIdAndPassword("admin", "admin!@#123");

        System.out.println(account);
    }
}
