package com.pit.auth.account.dto;

import com.pit.auth.account.model.Account;
import lombok.*;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
public class AccountDto {
    private String id;
    private String password;
    private String authority;

    public AccountDto(Account account){
        BeanUtils.copyProperties(account, this);
    }
}
