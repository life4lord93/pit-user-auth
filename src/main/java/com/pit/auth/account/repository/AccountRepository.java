package com.pit.auth.account.repository;

import com.pit.auth.account.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, String> {

    Account findByIdAndPassword(String id, String password);
}
