package com.pit.auth.account.service;

import com.pit.auth.account.dto.AccountDto;
import com.pit.auth.account.repository.AccountRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;

    public AccountDto getAccount(String id, String password) {
        return new AccountDto(accountRepository.findByIdAndPassword(id, password));
    }

}
