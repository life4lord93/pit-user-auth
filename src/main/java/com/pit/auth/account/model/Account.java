package com.pit.auth.account.model;

import com.pit.auth.account.dto.AccountDto;
import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
public class Account {
    @Id
    private String id;
    private String password;
    private String authority;
}
