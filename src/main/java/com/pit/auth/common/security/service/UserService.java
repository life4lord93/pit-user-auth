package com.pit.auth.common.security.service;

import com.pit.auth.account.dto.AccountDto;
import com.pit.auth.account.model.Account;
import com.pit.auth.account.repository.AccountRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@AllArgsConstructor
@Service
public class UserService implements UserDetailsService {

    private final AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {
        Account account = accountRepository.findById(id)
                .orElseThrow(() -> {throw new UsernameNotFoundException("not found user : " + id);});
        AccountDto accountDto = new AccountDto(account);

        return new User(accountDto.getId(), accountDto.getPassword(), Collections.singleton(new SimpleGrantedAuthority(accountDto.getAuthority())));
    }
}
